# php-dev-pre-tasks

Because roles don't have a pre_tasks function, we have to specify this role which is used to play before other dependency roles in [php-dev](https://gitlab.com/al-v-in_sys/ansible-roles/php-dev)

Specifically create the /var/www/html dir so apache install doesn't fail
